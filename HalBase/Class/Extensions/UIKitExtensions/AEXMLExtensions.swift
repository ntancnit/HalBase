//
//  AEXMLExtensions.swift
//  DMT
//
//  Created by Dao Duy Duong on 6/11/18.
//  Copyright © 2018 NGUYỄN THANH ÂN. All rights reserved.
//

import Foundation
import AEXML

extension AEXMLElement {
    
    func findElement(byName name: String, recursive: Bool = true) -> AEXMLElement? {
        let result: AEXMLElement? = children.flatMap { el in
            if el.name == (name) {
                return el
            }
            
            if recursive && el.children.count > 0 {
                return el.findElement(byName: name)
            }
            
            return nil
            }.first
        
        return result
    }
    
}
